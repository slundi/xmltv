use std::io::Write;

mod config;
mod feed;
mod filter;
mod ical;
mod reader;
mod time;
mod tui;

#[derive(Debug, Default)]
struct Cli {
    /// Wanted channel list, separated with a comma (`-c tf1,m6` or `--channels="france 2,Arte"` or `-c ?` to open the terminal UI)
    channels: Option<String>,

    /// List of what we want to remove from the file (`credits,new,category` for example). Use `--cleanse`
    cleanse: Option<String>,

    /// Output format
    format: Option<String>,

    /// Input XMLTV file you want to read
    file: String,
}

fn parse_args() -> Result<Cli, lexopt::Error> {
    use lexopt::prelude::*;

    let mut args: Cli = Cli::default();
    let mut parser = lexopt::Parser::from_env();

    while let Some(arg) = parser.next()? {
        match arg {
            Short('c') | Long("channels") => {
                args.channels = Some(parser.value()?.parse()?);
            }
            Short('f') | Long("format") => {
                let value: String = parser.value()?.parse()?;
                let value = value.to_lowercase();
                args.format = Some(value.clone());
                if !matches!(
                    value.as_str(),
                    "json" | "xml" | "ical" | "atom" | "feed" | "jsonfeed"
                ) {
                    // iCal, Atom
                    return Err(lexopt::Error::UnexpectedValue {
                        option: "format".to_owned(),
                        value: std::ffi::OsString::from(value),
                    });
                }
            }
            Long("cleanse") => {
                args.cleanse = Some(parser.value()?.parse()?);
            }
            Value(val) => {
                args.file = val.string()?;
            }
            Long("help") => {
                println!("Usage: xmltv [-c|--channels=\"Comma separated channels\"] FILE");
                std::process::exit(0);
            }
            _ => return Err(arg.unexpected()),
        }
    }

    Ok(args)
}

fn main() -> Result<(), lexopt::Error> {
    let args = parse_args()?;
    if args.file.is_empty() {
        eprintln!("XMLTV file was not provided");
        std::process::exit(1);
    }

    // cleanse
    let mut items_to_remove: Vec<String> = Vec::with_capacity(10);
    println!("Cleanse: {:?}", args.cleanse);
    if let Some(value) = args.cleanse {
        for v in value.split(",") {
            items_to_remove.push(v.trim().to_lowercase());
        }
    }

    let mut tv = reader::tv(&args.file);

    // filter channels
    let mut channels: Vec<xmltv::Channel> = Vec::new();
    if let Some(value) = args.channels {
        if value == "?" {
            match tui::channels::show(&reader::channels(&args.file, &[])) {
                Err(err) => {
                    eprintln!("Cannot show channel list: {:?}", err);
                    std::process::exit(1);
                }
                Ok(list) => {
                    let mut channel_filters: Vec<crate::filter::ChannelFilter> =
                        Vec::with_capacity(list.len());
                    for c in list {
                        channel_filters.push(filter::ChannelFilter::ById(c.id.to_lowercase()));
                    }
                    channels = reader::channels(&args.file, &channel_filters);
                }
            }
        } else {
            let list = value.split(',');
            let mut n = value.matches(',').count();
            if n == 0 {
                n = 1;
            }
            let mut channel_filters: Vec<crate::filter::ChannelFilter> = Vec::with_capacity(n);
            for c in list {
                channel_filters.push(filter::ChannelFilter::ByIdOrName(c.to_lowercase()));
            }
            if n == 0 {
                // when there is no comma, the split gives an empty list so we directly add the value
                channel_filters.push(filter::ChannelFilter::ByIdOrName(value.to_lowercase()));
            }
            channels = reader::channels(&args.file, &channel_filters);
        }
    }
    tv.channels = channels.clone();
    tv.programmes = reader::programmes(&args.file, &channels, &items_to_remove);
    if let Some(format) = args.format {
        match format.as_str() {
            "json" => serde_json::to_writer(&mut std::io::stdout(), &tv).unwrap(),
            "ical" => {
                let c = ical::Calendar {
                    prodid: "xmltv".to_owned(),
                    version: "1.0".to_owned(),
                    calscale: "GREGORIAN".to_owned(),
                    method: "REQUEST".to_owned(),
                    x_wr_calname: "XML TV".to_owned(),
                    x_wr_timezone: chrono::Local::now().offset().to_string(),
                    events: Vec::with_capacity(tv.programmes.len()),
                };
                c.write_ical(&mut std::io::stdout(), &tv).unwrap();
            }
            "atom" | "feed" => feed::write_atom(&mut std::io::stdout(), &tv).unwrap(),
            "jsonfeed" => feed::write_jsonfeed(&mut std::io::stdout(), &tv).unwrap(),
            "xml" => write!(
                &mut std::io::stdout(),
                "{}",
                quick_xml::se::to_string_with_root("tv", &tv).unwrap()
            )
            .unwrap(),
            _ => eprint!(
                "Unknow format {}. Choices are: json, ical, atom, xml (default)",
                format
            ),
        }
    } else {
        write!(
            &mut std::io::stdout(),
            "{}",
            quick_xml::se::to_string_with_root("tv", &tv).unwrap()
        )
        .unwrap();
    }
    Ok(())
}
