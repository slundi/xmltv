//! To load configuration from a file. It is alsoused to gather all options.

#[derive(Debug, Clone, PartialEq)]
pub struct Config {
    /// List of channel ID or name (in the `display-name` tag(s))
    pub channels: Vec<String>,

    /// List of elements that we want to remove from the result to have a lighter file or/and because we don't need it
    pub clean_elements: Vec<String>,
}
