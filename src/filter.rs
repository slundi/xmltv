use xmltv::{Channel, Programme};

#[derive(Debug)]
pub enum ChannelFilter {
    ById(String),
    ByIdOrName(String),
}

/// Perform filters and return a boolean telling whether we should keep it (in selected channel, day, time range, ...)
pub fn perform(
    programme: &mut Programme,
    channels: &[Channel],
    items_to_remove: &[String],
) -> bool {
    let mut channel_ids: Vec<String> = Vec::with_capacity(channels.len());
    for c in channels.iter() {
        channel_ids.push(c.id.clone());
    }
    if !channel_ids.contains(&programme.channel) {
        return false;
    }
    //TODO: remove programmes not in time range or day
    cleanse(programme, items_to_remove);
    true
}

fn cleanse(programme: &mut Programme, items_to_remove: &[String]) {
    for i in items_to_remove {
        let item = i.as_str();
        match item {
            "credits" => programme.credits = None,
            "directors" | "actors" | "writers" | "adapters" | "producers" | "composers"
            | "editors" | "presenters" | "commentators" | "guests" => {
                if let Some(credits) = programme.credits.as_mut() {
                    if item == "directors" {
                        credits.directors.clear();
                    }
                    if item == "actors" {
                        credits.actors.clear()
                    }
                    if item == "writers" {
                        credits.writers.clear()
                    }
                    if item == "adapters" {
                        credits.adapters.clear()
                    }
                    if item == "producers" {
                        credits.producers.clear()
                    }
                    if item == "composers" {
                        credits.composers.clear()
                    }
                    if item == "editors" {
                        credits.editors.clear()
                    }
                    if item == "presenters" {
                        credits.presenters.clear()
                    }
                    if item == "commentators" {
                        credits.commentators.clear()
                    }
                    if item == "guests" {
                        credits.guests.clear()
                    }
                }
            }
            "categories" => programme.categories.clear(),
            "new" => programme.new = false,
            "countries" => programme.countries.clear(),
            "keywords" => programme.keywords.clear(),
            "sub-titles" => programme.sub_titles.clear(),
            "languages" => programme.language = None,
            "videos" => programme.video = None,
            "audios" => programme.audio = None,
            "subtitles" => programme.subtitles.clear(),
            "last-chances" => programme.last_chance = None,
            "descriptions" => programme.descriptions.clear(),
            "dates" => programme.date = None,
            "origin-languages" => programme.orig_language = None,
            "length" => programme.length = None,
            "premieres" => programme.premiere = None,
            "previously-showns" => programme.previously_shown = None,
            "ratings" => programme.ratings.clear(),
            "star-ratings" => programme.star_ratings.clear(),
            "reviews" => programme.reviews.clear(),
            "images" => programme.images.clear(),
            "episode-nums" => programme.episode_num.clear(),
            "icons" => programme.icons.clear(),
            "urls" => programme.urls.clear(),
            _ => eprintln!("{} is not an item to remove", i),
        }
    }
}
