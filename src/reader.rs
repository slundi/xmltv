use std::io::BufRead;

use quick_xml::de::from_str;
use quick_xml::events::{BytesStart, Event};
use quick_xml::reader::Reader;
use quick_xml::Writer;
use xmltv::*;

/// Reads from a start tag all the way to the corresponding end tag, returns the bytes of the whole tag
fn read_to_end_into_buffer<R: BufRead>(
    reader: &mut Reader<R>,
    start_tag: &BytesStart,
    junk_buf: &mut Vec<u8>,
) -> Result<Vec<u8>, quick_xml::Error> {
    let mut depth = 0;
    let mut output_buf: Vec<u8> = Vec::new();
    let mut w = Writer::new(&mut output_buf);
    let tag_name = start_tag.name();
    w.write_event(Event::Start(start_tag.clone()))?;
    loop {
        junk_buf.clear();
        let event = reader.read_event_into(junk_buf)?;
        w.write_event(&event)?;

        match event {
            Event::Start(e) if e.name() == tag_name => depth += 1,
            Event::End(e) if e.name() == tag_name => {
                if depth == 0 {
                    return Ok(output_buf);
                }
                depth -= 1;
            }
            Event::Eof => {
                panic!("oh no")
            }
            _ => {}
        }
    }
}

/// Filter channels. If `filter` is empty, no filter is applied and it returns everything
pub fn channels(file: &str, filters: &[crate::filter::ChannelFilter]) -> Vec<Channel> {
    let mut result: Vec<Channel> = Vec::new();
    let xml = std::fs::read_to_string(file).unwrap();
    let mut reader = Reader::from_str(&xml);
    reader.trim_text(true);
    // let mut count = 1u16;
    let mut buf = Vec::new();
    let mut junk_buf: Vec<u8> = Vec::new();
    // The `Reader` does not implement `Iterator` because it outputs borrowed data (`Cow`s)
    loop {
        // NOTE: this is the generic case when we don't know about the input BufRead.
        // when the input is a &str or a &[u8], we don't actually need to use another
        // buffer, we could directly call `reader.read_event()`
        match reader.read_event_into(&mut buf) {
            Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
            // exits the loop when reaching end of file
            Ok(Event::Eof) => break,
            Ok(Event::Start(e)) => {
                if e.name().as_ref() == b"channel" {
                    let release_bytes =
                        read_to_end_into_buffer(&mut reader, &e, &mut junk_buf).unwrap();
                    let content = std::str::from_utf8(&release_bytes).unwrap();
                    let channel: Channel = from_str(content).unwrap();
                    // filter
                    if filters.is_empty() {
                        result.push(channel.clone());
                    } else {
                        let id_channel = channel.clone().id.to_lowercase();
                        for f in filters.iter() {
                            match f {
                                crate::filter::ChannelFilter::ById(id) => {
                                    if id.to_lowercase() == id_channel {
                                        result.push(channel.clone());
                                    }
                                }
                                crate::filter::ChannelFilter::ByIdOrName(value) => {
                                    let v = value.to_lowercase();
                                    if v == id_channel {
                                        result.push(channel.clone());
                                    } else {
                                        for n in channel.clone().display_names.iter() {
                                            if v == n.name.to_lowercase() {
                                                result.push(channel.clone());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // Ok(Event::Text(e)) => println!("Text: {}", e.unescape().unwrap()),
            // There are several other `Event`s we do not consider here
            _ => (),
        }
        // if we don't keep a borrow elsewhere, we can clear the buffer to keep memory usage low
        buf.clear();
    }
    result
}

pub fn programmes(file: &str, channels: &[Channel], items_to_remove: &[String]) -> Vec<Programme> {
    let mut result: Vec<Programme> = Vec::new();
    let xml = std::fs::read_to_string(file).unwrap();
    let mut reader = Reader::from_str(&xml);
    reader.trim_text(true);
    // let mut count = 1u16;
    let mut buf = Vec::new();
    let mut junk_buf: Vec<u8> = Vec::new();
    // The `Reader` does not implement `Iterator` because it outputs borrowed data (`Cow`s)
    loop {
        // NOTE: this is the generic case when we don't know about the input BufRead.
        // when the input is a &str or a &[u8], we don't actually need to use another
        // buffer, we could directly call `reader.read_event()`
        match reader.read_event_into(&mut buf) {
            Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
            // exits the loop when reaching end of file
            Ok(Event::Eof) => break,
            Ok(Event::Start(e)) => {
                if e.name().as_ref() == b"programme" {
                    let release_bytes =
                        read_to_end_into_buffer(&mut reader, &e, &mut junk_buf).unwrap();
                    let content = std::str::from_utf8(&release_bytes).unwrap();
                    let mut p: Programme = from_str(content).unwrap();
                    if crate::filter::perform(&mut p, channels, items_to_remove) {
                        println!("{:?}", p);
                        result.push(p);
                    }
                }
            }
            // Ok(Event::Text(e)) => println!("Text: {}", e.unescape().unwrap()),

            // There are several other `Event`s we do not consider here
            _ => (),
        }
        // if we don't keep a borrow elsewhere, we can clear the buffer to keep memory usage low
        buf.clear();
    }
    result
}

pub fn tv(file: &str) -> Tv {
    let mut result: Tv = Tv::default();
    let xml = std::fs::read_to_string(file).unwrap();
    let mut reader = Reader::from_str(&xml);
    reader.trim_text(true);
    // let mut count = 1u16;
    let mut buf = Vec::new();
    // The `Reader` does not implement `Iterator` because it outputs borrowed data (`Cow`s)
    loop {
        match reader.read_event_into(&mut buf) {
            Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
            Ok(Event::Eof) => break,
            Ok(Event::Start(e)) => {
                if e.name().as_ref() == b"tv" {
                    for attribute in e.attributes() {
                        match attribute {
                            Ok(a) => {
                                let key = std::str::from_utf8(a.key.0).unwrap();
                                let value = String::from_utf8(a.value.to_vec()).unwrap();
                                match key {
                                    "source-info-url" => result.source_info_url = Some(value),
                                    "source-info-name" => result.source_info_name = Some(value),
                                    "source-data-url" => result.source_data_url = Some(value),
                                    "generator-info-name" => {
                                        result.generator_info_name = Some(value)
                                    }
                                    "generator-info-url" => result.generator_info_url = Some(value),
                                    _ => panic!("Invalid attribute key root Tv tag: {}", key),
                                }
                            }
                            Err(e) => panic!("Error in Tv tag: {:?}", e),
                        }
                    }
                }
            }
            Ok(_) => break,
        }
        buf.clear();
    }
    result
}
