use serde::{Deserialize, Serialize};
use utils::*;

pub mod utils;

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct NameAndLang {
    #[serde(rename = "$text", skip_serializing_if = "String::is_empty")]
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none", rename = "@lang")]
    pub lang: Option<String>,
}

#[derive(Debug, PartialEq, Clone, Serialize, Default)]
pub struct ValueAndLang {
    #[serde(rename = "$text", skip_serializing_if = "String::is_empty")]
    pub value: String,
    #[serde(skip_serializing_if = "Option::is_none", rename = "@lang")]
    pub lang: Option<String>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct EmptyTag {}

/// Date should be the date when the listings were originally produced in whatever format; if you're converting data from another source, then
/// use the date given by that source.  The date when the conversion itself was done is not important.
///
/// To publicize your wonderful program which generated this file, you can use `generator-info-name` (preferably in the form 'progname/version')
/// and `generator-info-url` (a link to more info about the program).
///
/// ```dtd
/// <!ELEMENT tv (channel*, programme*)>
/// <!ATTLIST tv date   CDATA #IMPLIED
/// source-info-url     CDATA #IMPLIED
/// source-info-name    CDATA #IMPLIED
/// source-data-url     CDATA #IMPLIED
/// generator-info-name CDATA #IMPLIED
/// generator-info-url  CDATA #IMPLIED >
/// ```
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct Tv {
    /// `source-info-url` is a URL describing the data source in some human-readable form.  So if you are getting your listings from
    /// SAT.1, you might set this to the URL of a page explaining how to subscribe to their feed.  If you are getting them from a website, the
    /// URL might be the index of the site or at least of the TV listings section.
    #[serde(skip_serializing_if = "Option::is_none", rename = "@source-info-url")]
    pub source_info_url: Option<String>,
    /// `source-info-name` is the link text for that URL; it should generally be the human-readable name of your listings supplier.
    /// Sometimes the link text might be printed without the link itself, in hardcopy listings for example.
    #[serde(skip_serializing_if = "Option::is_none", rename = "@source-info-name")]
    pub source_info_name: Option<String>,
    /// `source-data-url' is where the actual data is grabbed from.  This should link directly to the machine-readable data files if possible,
    /// but it's not rigorously defined what 'actual data' means.  If you are parsing the data from human-readable pages, then it's more appropriate
    /// to link to them with the source-info stuff and omit this attribute.
    #[serde(skip_serializing_if = "Option::is_none", rename = "@source-data-url")]
    pub source_data_url: Option<String>,
    #[serde(
        skip_serializing_if = "Option::is_none",
        rename = "@generator-info-name"
    )]
    pub generator_info_name: Option<String>,
    #[serde(
        skip_serializing_if = "Option::is_none",
        rename = "@generator-info-url"
    )]
    pub generator_info_url: Option<String>,
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "channel"
    )]
    pub channels: Vec<Channel>,
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "programme"
    )]
    pub programmes: Vec<Programme>,
}

/// Structure representing a XMLTV channel.
/// Each 'programme' element (see below) should have an attribute 'channel' giving the channel on which it is broadcast.  If you want to
/// provide more detail about channels, you can give some 'channel' elements before listing the programmes.  The 'id' attribute of the
/// channel should match what is given in the 'channel' attribute of the programme.
///
/// Typically, all the channels used in a particular TV listing will be included and then the programmes using those channels.  But it's
/// entirely optional to include channel details - you can just leave out channel elements or provide only some of them.  It is also okay to
/// give just channels and no programmes, if you just want to describe what TV channels are available in a certain area.
///
// Each channel has one id attribute, which must be unique and should preferably be in the form suggested by RFC2838 (the 'broadcast'
/// element of the grammar in that RFC, in other words, a DNS-like name but without any URI scheme).  Then one or more display names which are
/// shown to the user.  You might want a different display name for different languages, but also you can have more than one name for the
/// same language.  Names listed earlier are considered 'more canonical'.
///
/// Since the display name is just there as a way for humans to refer to the channel, it's acceptable to just put the channel number if it's
/// fairly universal among viewers of the channel.  But remember that this isn't an official statement of what channel number has been
/// allocated, and the same number might be used for a different channel somewhere else.
///
/// The ordering of channel elements makes no difference to the meaning of the file, since they are looked up by id and not by their position.
/// However it makes things like diffing easier if you write the channel elements sorted by ASCII order of their ids.
///
/// ```dtd
/// <!ELEMENT channel (display-name+, icon*, url*) >
/// <!ATTLIST channel id CDATA #REQUIRED >
/// ```
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct Channel {
    #[serde(rename = "@id")]
    pub id: String,
    /// A user-friendly name for the channel - maybe even a channel number.  List the most canonical / common ones first and the most
    /// obscure names last.  The lang attribute follows RFC 1766.
    ///
    /// ```dtd
    /// <!ELEMENT display-name (#PCDATA)>
    /// <!ATTLIST display-name lang CDATA #IMPLIED>
    /// ```
    #[serde(
        rename = "display-name",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub display_names: Vec<NameAndLang>,
    #[serde(
        rename = "icon",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub icons: Vec<String>,
    #[serde(
        rename = "url",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub urls: Vec<Url>,
}

/// A URL where you can find out more about the element that contains it (programme or channel).  This might be the official site, or a fan
/// page, whatever you like really.
///
/// If multiple url elements are given, the most authoritative or official (which might conflict...) sites should be listed first.
///
/// If the URL does not define a real (i.e. clickable) link then the scheme should be set to something other than 'http://' such as 'uri://'
///
/// ```dtd
/// <!ELEMENT url (#PCDATA)>
/// <!ATTLIST url system CDATA #IMPLIED>
/// ```
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct Url {
    #[serde(rename = "$text")]
    pub value: String,
    /// The system attribute may be used to identify the source or target of the url, or some other useful feature of the target.
    #[serde(skip_serializing_if = "Option::is_none", rename = "@system")]
    pub system: Option<String>,
}

/// Structure representing a XMLTV program element:
///
/// ```dtd
/// <!ELEMENT programme (title+, sub-title*, desc*, credits?, date?,
///                      category*, keyword*, language?, orig-language?,
///                      length?, icon*, url*, country*, episode-num*,
///                      video?, audio?, previously-shown?, premiere?,
///                      last-chance?, new?, subtitles*, rating*,
///                      star-rating*, review*, image* )>
/// <!ATTLIST programme start     CDATA #REQUIRED
///                     stop      CDATA #IMPLIED
///                     pdc-start CDATA #IMPLIED
///                     vps-start CDATA #IMPLIED
///                     showview  CDATA #IMPLIED
///                     videoplus CDATA #IMPLIED
///                     channel   CDATA #REQUIRED
///                     clumpidx  CDATA "0/1" >
/// ```
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct Programme {
    // Attributes
    #[serde(rename = "@channel")]
    pub channel: String,
    #[serde(rename = "@start")]
    pub start: String,
    #[serde(skip_serializing_if = "Option::is_none", rename = "@stop")]
    pub stop: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none", rename = "@pdc-start")]
    pub pdc_start: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none", rename = "@vps-start")]
    pub vps_start: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub showview: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub videoplus: Option<String>,
    /// TV listings sometimes have the problem of listing two or more
    /// programmes in the same timeslot, such as 'News; Weather'.  We call
    /// this a 'clump' of programmes, and the 'clumpidx' attribute
    /// differentiates between two programmes sharing the same timeslot and
    /// channel.  In this case News would have clumpidx="0/2" and Weather
    /// would have clumpidx="1/2".  If you don't have this problem, be thankful!
    #[serde(skip_serializing_if = "Option::is_none")]
    pub clumpidx: Option<String>,
    // Children
    /// Structure representing a XMLTV title: Programme title, eg 'The Simpsons'.
    ///
    /// ```dtd
    /// <!ELEMENT title (#PCDATA)>
    /// <!ATTLIST title lang CDATA #IMPLIED>
    /// ```
    #[serde(
        rename = "title",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub titles: Vec<ValueAndLang>,
    /// Structure representing a XMLTV sub title: Sub-title or episode title, eg 'Datalore'. Should probably be
    /// called 'secondary title' to avoid confusion with captioning!
    ///
    /// ```dtd
    /// <!ELEMENT sub-title (#PCDATA)>
    /// <!ATTLIST sub-title lang CDATA #IMPLIED>
    /// ```
    #[serde(
        rename = "sub-title",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub sub_titles: Vec<ValueAndLang>,
    /// Structure representing a XMLTV description of the programme or episode.
    ///
    /// Unlike other elements, long bits of whitespace here are treated as equivalent to a single space and newlines are permitted, so you can
    /// break lines and write a pretty-looking paragraph if you wish.
    ///
    /// ```dtd
    /// <!ELEMENT desc (#PCDATA)>
    /// <!ATTLIST desc lang CDATA #IMPLIED>
    /// ```
    #[serde(
        rename = "desc",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub descriptions: Vec<ValueAndLang>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub credits: Option<Credits>,
    /// `<!ELEMENT date (#PCDATA)>`
    #[serde(skip_serializing_if = "Option::is_none")]
    pub date: Option<String>,
    /// XMLTV Program category. Type of programme, eg 'soap', 'comedy' or whatever the
    /// equivalents are in your language.  There's no predefined set of categories and it's okay for a programme to belong to several.
    ///
    /// ```dtd
    /// <!ELEMENT category (#PCDATA)>
    /// <!ATTLIST category lang CDATA #IMPLIED>
    /// ```
    #[serde(
        rename = "category",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub categories: Vec<NameAndLang>,
    /// Keywords for the programme, eg 'prison-drama', 'based-on-novel', 'super-hero'.  There's no predefined set of keywords and it's likely
    /// for a programme to have several.  It is recommended that keywords containing multiple words are hyphenated.
    ///
    /// ```dtd
    /// <!ELEMENT keyword (#PCDATA)>
    /// <!ATTLIST keyword lang CDATA #IMPLIED>
    /// ```
    #[serde(
        rename = "keyword",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub keywords: Vec<ValueAndLang>,
    /// The language the programme will be broadcast in.  This does not include the language of any subtitles, but it is affected by dubbing
    /// into a different language.  For example, if a French film is dubbed into English, language=en and orig-language=fr.
    ///
    /// There are two ways to specify the language.  You can use the two-letter codes such as en or fr, or you can give a name such as
    /// 'English' or 'Deutsch'.  In the latter case you might want to use the 'lang' attribute, for example: `<language lang="fr">Allemand</language>`.
    ///
    /// ```dtd
    /// <!ELEMENT language (#PCDATA)>
    /// <!ATTLIST language lang CDATA #IMPLIED>
    /// ```
    #[serde(rename = "language", skip_serializing_if = "Option::is_none")]
    pub language: Option<ValueAndLang>,
    /// The original language, before dubbing.  The same remarks as for 'language' apply.
    ///
    /// ```dtd
    /// <!ELEMENT orig-language (#PCDATA)>
    /// <!ATTLIST orig-language lang CDATA #IMPLIED>
    /// ```
    #[serde(rename = "orig-language", skip_serializing_if = "Option::is_none")]
    pub orig_language: Option<ValueAndLang>,
    #[serde(skip_serializing_if = "Option::is_none", default)]
    pub length: Option<Length>,
    #[serde(
        rename = "icon",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub icons: Vec<Icon>,
    #[serde(
        rename = "url",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub urls: Vec<Url>,
    /// A country where the programme was made or one of the countries in a joint production.  You can give the name of a country, in which case
    /// you might want to specify the language in which this name is written, or you can give a two-letter uppercase country code, in which case the
    /// lang attribute should not be given.  For example: `<country lang="en">Italy</country><country>GB</country>`
    ///
    /// ```dtd
    /// <!ELEMENT country (#PCDATA)>
    /// <!ATTLIST country lang CDATA #IMPLIED>
    /// ```
    #[serde(
        rename = "country",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub countries: Vec<NameAndLang>,
    #[serde(
        rename = "episode-num",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub episode_num: Vec<EpisodeNum>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub video: Option<Video>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub audio: Option<Audio>,
    #[serde(skip_serializing_if = "Option::is_none", rename = "previously-show")]
    pub previously_shown: Option<PreviouslyShown>,
    /// Different channels have different meanings for this word - sometimes it means a film has never before been seen on TV in
    /// that country, but other channels use it to mean 'the first showing of this film on our channel in the current run'.  It might have been
    /// shown before, but now they have paid for another set of showings, which makes the first in that set count as a premiere!
    ///
    /// So this element doesn't have a clear meaning, just use it to represent where 'premiere' would appear in a printed TV listing.  You can use
    /// the content of the element to explain exactly what is meant, for example:
    /// ```xml
    /// <premiere lang="en">
    ///   First showing on national terrestrial TV
    /// </premiere>
    /// ```
    ///
    /// The textual content is a 'paragraph' as for <desc>.  If you don't want to give an explanation, just write empty content: `<premiere />`
    ///
    /// ```dtd
    /// <!ELEMENT premiere (#PCDATA)>
    /// <!ATTLIST premiere lang CDATA #IMPLIED>
    /// ```
    #[serde(skip_serializing_if = "Option::is_none")]
    pub premiere: Option<ValueAndLang>,
    /// In a way this is the opposite of premiere.  Some channels buy the rights to show a movie a certain number of times, and
    /// the first may be flagged 'premiere', the last as 'last showing'.
    ///
    /// For symmetry with premiere, you may use the element content to give a 'paragraph' describing exactly what is meant - it's unlikely to be the
    /// last showing ever!  Otherwise, explicitly put empty content: `<last-chance />`
    ///
    /// ```dtd
    /// <!ELEMENT last-chance (#PCDATA)>
    /// <!ATTLIST last-chance lang CDATA #IMPLIED>
    /// ```
    #[serde(rename = "last-chance", skip_serializing_if = "Option::is_none")]
    pub last_chance: Option<ValueAndLang>,
    /// This is the first screened programme from a new show that has never been shown on television before - if not worldwide then at
    /// least never before in this country.  After the first episode or programme has been shown, subsequent ones are no longer 'new'.
    /// Similarly the second series of an established programme is not 'new'.
    ///
    /// Note that this does not mean 'new season' or 'new episode' of an existing show.  You can express part of that using the episode-num stuff.
    ///
    /// `<!ELEMENT new EMPTY>`
    #[serde(
        skip_serializing_if = "std::ops::Not::not",
        serialize_with = "bool_to_new_tag",
        deserialize_with = "new_tag_to_boolean",
        default
    )]
    pub new: bool, // TODO: as boolean, default false
    #[serde(skip_serializing_if = "Vec::is_empty", default = "Vec::new")]
    pub subtitles: Vec<Subtitles>,
    #[serde(
        rename = "rating",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub ratings: Vec<Rating>,
    #[serde(
        rename = "star-rating",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub star_ratings: Vec<StarRating>,
    #[serde(
        rename = "review",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub reviews: Vec<Review>,
    #[serde(
        rename = "image",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub images: Vec<Icon>,
}

/// XMLTV Program actor
///
/// ```dtd
/// <!ATTLIST actor role  CDATA      #IMPLIED
///                 guest (no | yes) #IMPLIED >
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default)]
pub struct Actor {
    #[serde(rename = "$text")]
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none", rename = "@role")]
    pub role: Option<String>,
    #[serde(
        rename = "@guest",
        serialize_with = "bool_to_yes_no",
        deserialize_with = "yes_no_to_bool",
        skip_serializing_if = "std::ops::Not::not",
        default
    )]
    pub guest: bool,
}

/// XMLTV Program credits of the programme.
///
/// People are listed in decreasing order of importance; so for example the starring actors appear first followed by the smaller parts.  As
/// with other parts of this file format, not mentioning a particular actor (for example) does not imply that he _didn't_ star in the film -
/// so normally you'd list only the few most important people.
///
/// Adapter can be either somebody who adapted a work for television, or somebody who did the translation from another language.  Maybe these
/// should be separate, but if so how would 'translator' fit in with the 'language' element?
///
/// URL can be, for example, a link to a webpage with more information about the actor, director, etc..
///
/// ```dtd
/// <!ELEMENT credits (director*, actor*, writer*, adapter*, producer*,
///                    composer*, editor*, presenter*, commentator*,
///                    guest* )>
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default)]
pub struct Credits {
    /// `<!ELEMENT director    (#PCDATA | image | url)* >`
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "director"
    )]
    pub directors: Vec<String>,
    /// `<!ELEMENT actor       (#PCDATA | image | url)* >`
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "actor"
    )]
    pub actors: Vec<Actor>,
    /// `<!ELEMENT writer      (#PCDATA | image | url)* >`
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "writer"
    )]
    pub writers: Vec<String>,
    /// `<!ELEMENT adapter     (#PCDATA | image | url)* >`
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "adapter"
    )]
    pub adapters: Vec<String>,
    /// `<!ELEMENT producer    (#PCDATA | image | url)* >`
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "producer"
    )]
    pub producers: Vec<String>,
    /// `<!ELEMENT composer    (#PCDATA | image | url)* >`
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "composer"
    )]
    pub composers: Vec<String>,
    /// `<!ELEMENT editor      (#PCDATA | image | url)* >`
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "editor"
    )]
    pub editors: Vec<String>,
    /// `<!ELEMENT presenter   (#PCDATA | image | url)* >`
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "presenter"
    )]
    pub presenters: Vec<String>,
    /// `<!ELEMENT commentator (#PCDATA | image | url)* >`
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "commentator"
    )]
    pub commentators: Vec<String>,
    /// `<!ELEMENT guest       (#PCDATA | image | url)* >`
    #[serde(
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new",
        rename = "guest"
    )]
    pub guests: Vec<String>,
}

/// ```dtd
/// <!ELEMENT episode-num (#PCDATA)>
/// <!ATTLIST episode-num system CDATA "onscreen">
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct EpisodeNum {
    #[serde(rename = "$text")]
    pub value: String,
    /// The system attribute may be used to identify the source or target of the url, or some other useful feature of the target.
    #[serde(rename = "@system")]
    pub system: String,
}

/// Video details: the subelements describe the picture quality as follows.
///
/// ```dtd
/// <!ELEMENT video (present?, colour?, aspect?, quality?)>
/// <!ELEMENT present (#PCDATA)>
/// <!ELEMENT colour (#PCDATA)>
/// <!ELEMENT aspect (#PCDATA)>
/// <!ELEMENT quality (#PCDATA)>
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default)]
pub struct Video {
    /// Whether this programme has a picture (no, in the case of radio stations broadcast on TV or 'Blue'), legal values are
    /// 'yes' or 'no'.  Obviously if the value is 'no', the other elements are meaningless.
    pub present: Option<TagWithOnlyText>, // TODO: flatten & parse
    /// 'yes' for colour, 'no' for black-and-white.
    pub colour: Option<TagWithOnlyText>, // TODO: flatten & parse
    /// The horizontal:vertical aspect ratio, eg `4:3` or `16:9`.
    pub aspect: Option<TagWithOnlyText>,
    /// Information on the quality, eg `HDTV`, `800x600`.
    pub quality: Option<TagWithOnlyText>,
}

/// Contains only the value in a tag: `<tag>value</tag>`.
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default)]
pub struct TagWithOnlyText {
    #[serde(rename = "$text")]
    pub value: String,
}

/// Audio details, similar to video details above.
///
/// ```dtd
/// <!ELEMENT audio (present?, stereo?)>
/// <!ELEMENT stereo (#PCDATA)>
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default)]
pub struct Audio {
    /// Whether this programme has any sound at all, 'yes' or 'no'.
    #[serde(rename = "@present", skip_serializing_if = "Option::is_none")]
    pub present: Option<bool>,
    /// Description of the stereo-ness of the sound.  Legal values are currently 'mono','stereo','dolby','dolby digital','bilingual'
    /// and 'surround'. 'bilingual' in this case refers to a single audio stream where the left and right channels contain monophonic audio
    /// in different languages.  Other values may be added later.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stereo: Option<TagWithOnlyText>, // TODO: flatten 1 level
}

/// When and where the programme was last shown, if known.  Normally in TV listings 'repeat' means 'previously shown on this channel', but
/// if you don't know what channel the old screening was on (but do know that it happened) then you can omit the 'channel' attribute.
/// Similarly you can omit the 'start' attribute if you don't know when the previous transmission was (though you can of course give just the
/// year, etc.).
///
/// The absence of this element does not say for certain that the programme is brand new and has never been screened anywhere before.
///
/// ```dtd
/// <!ELEMENT previously-shown EMPTY>
/// <!ATTLIST previously-shown start   CDATA #IMPLIED
///                            channel CDATA #IMPLIED >
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default)]
pub struct PreviouslyShown {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub start: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub channel: Option<String>,
}

/// These can be either 'teletext' (sent digitally, and displayed at the viewer's request), 'onscreen' (superimposed on the
/// picture and impossible to get rid of), or 'deaf-signed' (in-vision signing for users of sign language). You can have multiple subtitle
/// streams to handle different languages.  Language for subtitles is specified in the same way as for programmes.
///
/// ```dtd
/// <!ELEMENT subtitles (language?)>
/// <!ATTLIST subtitles type (teletext | onscreen | deaf-signed) #IMPLIED>
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default)]
pub struct Subtitles {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub language: Option<ValueAndLang>,
    // TODO: constraint values
    #[serde(rename = "@type", skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>, // TODO: enum
}

/// Various bodies decide on classifications for films - usually a minimum age you must be to see it.  In principle the same
/// could be done for ordinary TV programmes.  Because there are many systems for doing this, you can also specify the rating system used
/// (which in practice is the same as the body which made the rating).
///
/// ```dtd
/// <!ELEMENT rating (value, icon*)>
/// <!ATTLIST rating system CDATA #IMPLIED>
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default)]
pub struct Rating {
    pub value: String, // TODO:
    #[serde(
        rename = "icon",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub icons: Vec<Icon>,
    #[serde(rename = "@system", skip_serializing_if = "Option::is_none")]
    pub system: Option<String>,
}

/// many listings guides award a programme a score as a quick guide to how good it is.  The value of this element should be
/// 'N / M', for example one star out of a possible five stars would be '1 / 5'.  Zero stars is also a possible score (and not the same as
/// 'unrated').  You should try to map whatever wacky system your listings source uses to a number of stars: so for example if they have thumbs
/// up, thumbs sideways and thumbs down, you could map that to two, one or zero stars out of two.  If a programme is marked as recommended in a
/// listings guide you could map this to '1 / 1'. Because there could be many ways to provide star-ratings or recommendations for a programme, you can
/// specify multiple star-ratings. You can specify the star-rating system used, or the provider of the recommendation, with the system attribute.
/// Whitespace between the numbers and slash is ignored.
///
/// ```dtd
/// <!ELEMENT star-rating (value, icon*)>
/// <!ATTLIST star-rating system CDATA #IMPLIED>
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default)]
pub struct StarRating {
    pub value: String, // TODO:
    #[serde(
        rename = "icon",
        skip_serializing_if = "Vec::is_empty",
        default = "Vec::new"
    )]
    pub icons: Vec<Icon>,
    #[serde(rename = "@system", skip_serializing_if = "Option::is_none")]
    pub system: Option<String>,
}

/// Listings guides may provide reviews of programmes in addition to, or in place of, standard programme descriptions. They are
/// usually written by in-house reviewers, but reviews can also be made available by third-party organisations/individuals. The value of this
/// element must be either the text of the review, or a URL that links to it. Optional attributes giving the review source and the individual reviewer
/// can also be specified.
///
/// ```dtd
/// <!ELEMENT review (#PCDATA)>
/// <!ATTLIST review type     (text | url) #REQUIRED
///                  source   CDATA        #IMPLIED
///                  reviewer CDATA        #IMPLIED
///                  lang     CDATA        #IMPLIED>
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default)]
pub struct Review {
    #[serde(rename = "$text")]
    pub value: String,
    #[serde(rename = "@type")]
    pub r#type: String,
    #[serde(rename = "@source", skip_serializing_if = "Option::is_none")]
    pub source: Option<String>,
    #[serde(rename = "@reviewer", skip_serializing_if = "Option::is_none")]
    pub reviewer: Option<String>,
    #[serde(rename = "@lang", skip_serializing_if = "Option::is_none")]
    pub lang: Option<String>,
}

/// An icon associated with the element that contains it.
/// - src: uri of image
/// - width, height: (optional) dimensions of image
///
/// These dimensions are pixel dimensions for the time being, eventually this will change to be more like HTML's 'img'.
///
/// ```dtd
/// <!ELEMENT icon EMPTY>
/// <!ATTLIST icon src         CDATA #REQUIRED
/// width       CDATA #IMPLIED
/// height      CDATA #IMPLIED>
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Default)]
pub struct Icon {
    #[serde(rename = "@src")]
    pub src: String,
    #[serde(skip_serializing_if = "Option::is_none", rename = "@width")]
    pub width: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none", rename = "@height")]
    pub height: Option<String>,
}

/// The true length of the programme, not counting advertisements or trailers.  But this does take account of any bits which were cut out
///of the broadcast version - eg if a two hour film is cut to 110 minutes and then padded with 20 minutes of advertising, length will be 110
/// minutes even though end time minus start time is 130 minutes.
///
/// ```dtd
/// <!ELEMENT length (#PCDATA)>
/// <!ATTLIST length units (seconds | minutes | hours) #REQUIRED>
/// ```
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Length {
    #[serde(rename = "$text")]
    pub length: u16,
    #[serde(rename = "@units")]
    pub units: Units,
}

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
#[repr(u8)]
pub enum Units {
    Seconds,
    Minutes,
    Hours,
}
impl Length {
    /// Get a tuple of hours, minutes and seconds
    pub fn to_hms(&self) -> (u8, u8, u8) {
        match self.units {
            Units::Seconds => {
                let (m, s) = divmod(self.length, 60);
                let (h, m) = divmod(m.into(), 60);
                (h, m, s)
            }
            Units::Minutes => {
                let (h, m) = divmod(self.length, 60);
                (h, m, 0)
            }
            Units::Hours => (self.length.try_into().unwrap(), 0, 0),
        }
    }
}

impl std::fmt::Display for Units {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Units::Seconds => fmt.write_str("seconds"),
            Units::Minutes => fmt.write_str("minutes"),
            Units::Hours => fmt.write_str("hours"),
        }
    }
}

/// Takes two numbers as arguments and returns their quotient and remainder when using integer division.
fn divmod(x: u16, y: u16) -> (u8, u8) {
    let quotient = x / y;
    let remainder = x % y;
    (quotient.try_into().unwrap(), remainder.try_into().unwrap())
}
