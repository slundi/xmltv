use serde_json::json;

fn get_unique_uuid(channel: &xmltv::Channel, programme: &xmltv::Programme) -> uuid::Uuid {
    let mut buf: Vec<u8> = bincode::serialize(&channel.id.clone()).unwrap();
    buf.append(&mut bincode::serialize(&programme.start.clone()).unwrap());
    buf.append(&mut bincode::serialize(&programme.titles.clone().first().unwrap().value).unwrap());
    uuid::Uuid::new_v8(xxhash_rust::xxh3::xxh3_128(&buf).to_ne_bytes())
}

fn get_channel_map(
    channels: &Vec<xmltv::Channel>,
) -> std::collections::HashMap<&str, xmltv::Channel> {
    xmltv::utils::channels_to_map(channels)
}

pub fn write_atom<W: std::io::Write>(writer: &mut W, tv: &xmltv::Tv) -> std::io::Result<()> {
    writeln!(
        writer,
        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<feed xmlns=\"http://www.w3.org/2005/Atom\">"
    )?;
    writeln!(writer, "<title>From XMLTV</title>")?;
    writeln!(
        writer,
        "<updated>{}</updated>",
        chrono::offset::Utc::now().to_rfc3339()
    )?;
    let channels = get_channel_map(&tv.channels);
    for p in tv.programmes.iter() {
        writeln!(writer, "\t<entry>")?;
        let channel = channels.get(p.channel.as_str()).unwrap();
        writeln!(writer, "\t<id>{}</id>", get_unique_uuid(&channel, p))?;
        writeln!(
            writer,
            "\t<title>{} - {}</title>",
            channel.display_names.first().unwrap().name,
            p.titles.first().unwrap().value
        )?;
        if let Some(st) = p.sub_titles.first() {
            writeln!(writer, "\t<summary>{}</summary>", st.value)?;
        }
        if let Some(desc) = p.descriptions.first() {
            writeln!(writer, "\t<content>{}</content>", desc.value)?;
        }
        writeln!(writer, "\t</entry>")?;
    }
    write!(writer, "</feed>")?;
    Ok(())
}

pub fn write_jsonfeed<W: std::io::Write>(writer: &mut W, tv: &xmltv::Tv) -> std::io::Result<()> {
    writeln!(writer, "{{\n\"version\": \"https://jsonfeed.org/version/1\",\n\"title\": \"From XMLTV\",\n\"items\": [")?;
    let channels = get_channel_map(&tv.channels);
    let n = tv.programmes.len();
    let mut i = 1usize;
    for p in tv.programmes.iter() {
        let channel = channels.get(p.channel.as_str()).unwrap();
        writeln!(
            writer,
            "\t{{\n\t\t\"id\": \"{}\",",
            get_unique_uuid(&channel, p)
        )?;
        let mut title = channel.display_names.first().unwrap().name.clone();
        title.push_str(" - ");
        title.push_str(&p.titles.first().unwrap().value);
        write!(writer, "\t\t\"title\": {}", json!(title))?;
        if let Some(st) = p.sub_titles.first() {
            write!(writer, ",\n\t\t\"summary\": {}", json!(st.value))?;
        }
        if let Some(desc) = p.descriptions.first() {
            write!(writer, ",\n\t\t\"content\": {}", json!(desc.value))?;
        }
        if i < n {
            writeln!(writer, "\n\t}},")?;
        } else {
            writeln!(writer, "\n\t}}")?;
        }
        i += 1;
    }
    write!(writer, "]\n}}")?;
    Ok(())
}
