use chrono::DateTime;
use chrono::Utc;

/// Store all events from iCalendar.
#[derive(Debug, Clone, PartialEq)]
pub struct Event {
    pub dtstart: DateTime<Utc>,
    pub dtend: DateTime<Utc>,
    pub dtstamp: DateTime<Utc>,
    pub uid: String,
    pub created: DateTime<Utc>,
    pub description: String,
    pub last_modified: DateTime<Utc>,
    pub location: String,
    pub sequence: u32,
    pub status: String,
    pub summary: String,
    pub transp: String,
}

/// Store the iCalendar and add events from struct `Events`.
#[derive(Clone)]
pub struct Calendar {
    pub prodid: String,
    pub version: String,
    pub calscale: String,
    pub method: String,
    pub x_wr_calname: String,
    pub x_wr_timezone: String,
    pub events: Vec<Event>,
}

impl Event {
    /// Create an empty event
    pub fn empty() -> Event {
        let date_tz = Utc::now();
        Event {
            dtstart: date_tz,
            dtend: date_tz,
            dtstamp: date_tz,
            uid: "NULL".to_string(),
            created: date_tz,
            description: "NULL".to_string(),
            last_modified: date_tz,
            location: "NULL".to_string(),
            sequence: 0,
            status: "NULL".to_string(),
            summary: "NULL".to_string(),
            transp: "NULL".to_string(),
        }
    }

    pub(super) fn write_ical<W: std::io::Write>(&self, writer: &mut W) -> std::io::Result<()> {
        write!(writer, "BEGIN:VEVENT\r\n")?;
        write!(
            writer,
            "DTSTART:{}\r\n",
            &self.dtstart.format("%Y%m%dT%H%M%SZ")
        )?;
        write!(writer, "DTEND:{}\r\n", &self.dtend.format("%Y%m%dT%H%M%SZ"))?;
        write!(
            writer,
            "DTSTAMP:{}\r\n",
            &self.dtstamp.format("%Y%m%dT%H%M%SZ")
        )?;
        write!(writer, "UID:{}\r\n", &self.uid)?;
        write!(
            writer,
            "CREATED:{}\r\n",
            &self.created.format("%Y%m%dT%H%M%SZ")
        )?;
        write!(writer, "DESCRIPTION:{}\r\n", &self.description)?;
        // X-ALT-DESC;FMTTYPE=text/html:<!doctype html><html><body><h1 style="color:blue">Awesome event</h1><p>Please join</p></body></html>
        write!(
            writer,
            "LAST-MODIFIED:{}\r\n",
            &self.last_modified.format("%Y%m%dT%H%M%SZ")
        )?;
        write!(writer, "LOCATION:{}\r\n", &self.location)?;
        write!(writer, "SEQUENCE:{}\r\n", &self.sequence)?;
        write!(writer, "STATUS:{}\r\n", &self.status)?;
        write!(writer, "SUMMARY:{}\r\n", &self.summary)?;
        write!(writer, "TRANSP:{}\r\n", &self.transp)?;
        write!(writer, "END:VEVENT\r\n")?;
        Ok(())
    }

    pub(super) fn from_programme_and_channels(
        programme: &xmltv::Programme,
        channels: &Vec<xmltv::Channel>,
    ) -> Self {
        let mut e = Event::empty();
        let p = programme.clone();
        for c in channels {
            if programme.channel == c.id {
                e.uid = c.display_names.first().unwrap().name.clone();
                e.summary = p.titles.first().unwrap().value.clone();
                e.description = p.descriptions.first().unwrap().value.clone();
                break;
            }
        }
        e
    }
}

impl Calendar {
    /// Export iCalendar to any `Write` implementer.
    ///
    /// Example to stdout:
    /// ```rust
    /// ical.export_to(&mut std::io::stdout()).expect("Could not export to stdout");
    /// ```
    pub fn write_ical<W: std::io::Write>(
        &self,
        writer: &mut W,
        tv: &xmltv::Tv,
    ) -> std::io::Result<()> {
        write!(writer, "BEGIN:VCALENDAR\r\n")?;
        write!(writer, "PRODID:{}\r\n", &self.prodid)?;
        write!(writer, "CALSCALE:{}\r\n", &self.calscale)?;
        write!(writer, "VERSION:{}\r\n", &self.version)?;
        write!(writer, "METHOD:{}\r\n", &self.method)?;
        write!(writer, "X-WR-CALNAME:{}\r\n", &self.x_wr_calname)?;
        write!(writer, "X-WR-TIMEZONE:{}\r\n", &self.x_wr_timezone)?;
        for p in &tv.programmes {
            let e = Event::from_programme_and_channels(p, &tv.channels);
            e.write_ical(writer).unwrap();
        }
        write!(writer, "END:VCALENDAR")?;
        Ok(())
    }
}
