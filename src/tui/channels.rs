use crossterm::event::{self, KeyEventKind};
use ratatui::{prelude::*, widgets::*};

use xmltv::Channel;

// example: https://github.com/ratatui-org/ratatui/blob/main/examples/scrollbar.rs
// https://github.com/Beastwick18/nyaa/blob/main/src/app.rs
#[derive(Debug)]
struct App {
    pub shoud_quit: bool,
    pub scroll_state: ScrollbarState,
    pub scroll: usize,
    pub state: TableState,
    pub list: Vec<(bool, Channel)>,
}
impl App {
    pub fn toggle_check(&mut self, index: usize) {
        self.list[index].0 = !self.list[index].0;
    }
}

fn generate_rows(list: &Vec<(bool, Channel)>) -> Vec<Row> {
    let mut rows: Vec<Row> = Vec::with_capacity(list.len());
    for c in list {
        // println!("{:?}", c.id);
        let mut name = String::with_capacity(4 + c.1.display_names[0].name.len());
        name.push_str(if c.0 { "[x] " } else { "[ ] " });
        name.push_str(&c.1.display_names[0].name.clone());
        rows.push(Row::new(vec![name, c.1.id.clone()]));
    }
    rows
}

/// Generate the TUI table
fn render_table(f: &mut Frame, app: &mut App, area: Rect) {
    let table = Table::new(
        generate_rows(&app.list),
        [Constraint::Percentage(40), Constraint::Fill(1)],
    )
    .block(
        Block::bordered()
            .title(format!(
                "XMLTV: Channel selection: {} available",
                app.list.len()
            ))
            .title_alignment(Alignment::Center)
            .title_bottom(Line::from("↑↓ to move, Space to check/uncheck").left_aligned())
            .title_bottom(Line::from("Esc/Q to quit").right_aligned()),
    )
    .highlight_style(Style::new().add_modifier(Modifier::REVERSED)) // FIXME: not displayed with scrollbar, do not scroll
    .highlight_symbol(">>")
    .highlight_spacing(HighlightSpacing::Always)
    .header(Row::new(vec!["Name", "ID"]).style(Style::new().bold()));
    f.render_stateful_widget(table, area, &mut app.state);
}

fn render_scrollbar(f: &mut Frame, app: &mut App, area: Rect) {
    f.render_stateful_widget(
        Scrollbar::default()
            .orientation(ScrollbarOrientation::VerticalRight)
            .begin_symbol(None)
            .end_symbol(None),
        area.inner(&Margin {
            vertical: 1,
            horizontal: 1,
        }),
        &mut app.scroll_state,
    );
}

/// Renders the user interface widgets
fn render(app: &mut App, frame: &mut Frame) {
    let rects = Layout::vertical([Constraint::Fill(1)]).split(frame.size());
    render_table(frame, app, rects[0]);
    render_scrollbar(frame, app, rects[0]);
}

pub fn show(list: &Vec<Channel>) -> std::io::Result<Vec<xmltv::Channel>> {
    let mut terminal = crate::tui::get_terminal()?;
    // method) so that the selected row is preserved across renders
    let mut app = App {
        shoud_quit: false,
        scroll_state: ScrollbarState::default().content_length(list.len()),
        scroll: 0,
        state: TableState::default(),
        list: crate::tui::build_checkable_list(list),
    };
    let mut state = TableState::default();
    if list.len() > 0 {
        state.select(Some(0));
    }
    loop {
        // terminal.draw(|f| ui(f, &mut app))?;
        // draw the UI
        // terminal.draw(|frame| {
        //     let area = frame.size();
        //     frame.render_stateful_widget(generate_table(&checkable_list), area, &mut state);
        // })?;
        terminal.draw(|frame| render(&mut app, frame))?;
        // handle events
        if event::poll(std::time::Duration::from_millis(16))? {
            if let event::Event::Key(key) = event::read()? {
                if key.kind == KeyEventKind::Press
                    && (key.code == event::KeyCode::Esc || key.code == event::KeyCode::Char('q'))
                {
                    app.shoud_quit = true;
                    break;
                } else if key.kind == KeyEventKind::Press && key.code == event::KeyCode::Char(' ') {
                    app.toggle_check(state.selected().unwrap());
                } else if key.kind == KeyEventKind::Press && key.code == event::KeyCode::Up {
                    if let Some(index) = state.selected() {
                        if index > 0 {
                            state.select(Some(index - 1));
                            app.scroll = app.scroll.saturating_sub(1);
                            app.scroll_state = app.scroll_state.position(app.scroll);
                        } else {
                            // when in the beginning of the list, it goes down to the end
                            state.select(Some(app.list.len() - 1));
                            app.scroll = app.list.len() - 1;
                            app.scroll_state = app.scroll_state.position(app.scroll);
                        }
                    }
                } else if key.kind == KeyEventKind::Press && key.code == event::KeyCode::Down {
                    if let Some(index) = state.selected() {
                        if index < app.list.len() - 1 {
                            state.select(Some(index + 1));
                            app.scroll = app.scroll.saturating_add(1);
                            app.scroll_state = app.scroll_state.position(app.scroll);
                        } else {
                            // when in the end of the list, it goes back to the beginning
                            state.select(Some(0));
                            app.scroll = 0;
                            app.scroll_state = app.scroll_state.position(app.scroll);
                        }
                    }
                } else if key.kind == KeyEventKind::Press && key.code == event::KeyCode::Char('a') {
                    crate::tui::set_all_check_state(&mut app.list, true);
                } else if key.kind == KeyEventKind::Press && key.code == event::KeyCode::Char('n') {
                    crate::tui::set_all_check_state(&mut app.list, false);
                } else if key.kind == KeyEventKind::Press && key.code == event::KeyCode::Enter {
                    break;
                }
            }
        }
    }
    crate::tui::restore()?;
    if app.shoud_quit {
        return Ok(Vec::new());
    }
    let mut result: Vec<Channel> = Vec::with_capacity(list.len());
    for c in app.list.iter() {
        if c.0 {
            result.push(c.1.clone());
        }
    }
    Ok(result)
}
