use crossterm::{
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
    ExecutableCommand,
};
use ratatui::prelude::{CrosstermBackend, Terminal};

use std::io::{stdout, Stdout};

pub(crate) mod channels;
pub(crate) mod epg;
pub(crate) mod search;

/// Restore the terminal to its original state
pub fn restore() -> std::io::Result<()> {
    stdout().execute(LeaveAlternateScreen)?;
    disable_raw_mode()?;
    Ok(())
}

/// Get a terminal
pub fn get_terminal() -> std::io::Result<Terminal<CrosstermBackend<Stdout>>> {
    stdout().execute(EnterAlternateScreen)?;
    enable_raw_mode()?;
    let mut terminal = Terminal::new(CrosstermBackend::new(stdout()))?;
    terminal.clear()?;
    Ok(terminal)
}

/// Build a "checkable list" for a Vec of element by creating a Vec of tuple.
/// Rhe first element in the tuple is a boolean, the second is the element.
pub fn build_checkable_list<T: Clone>(list: &Vec<T>) -> Vec<(bool, T)> {
    let mut result: Vec<(bool, T)> = Vec::with_capacity(list.len());
    for e in list.iter() {
        result.push((false, e.clone()));
    }
    result
}

/// Change the first element of the tuple to the desired state for all items in the list.
pub fn set_all_check_state<T>(list: &mut Vec<(bool, T)>, state: bool) {
    for e in list.iter_mut() {
        e.0 = state;
    }
}

// /// Render a scollbar for items that needs it
// pub fn render_scrollbar(f: &mut Frame, app: &mut App, area: Rect) {
//     f.render_stateful_widget(
//         Scrollbar::default()
//             .orientation(ScrollbarOrientation::VerticalRight)
//             .begin_symbol(None)
//             .end_symbol(None),
//         area.inner(&Margin {
//             vertical: 1,
//             horizontal: 1,
//         }),
//         &mut app.scroll_state,
//     );
// }
