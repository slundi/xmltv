use chrono::format::ParseError;
use chrono::NaiveDateTime;

const PATTERN: &'static str = "%Y%m%d%H%M%S";
const PATTERN_WITH_TZ: &'static str = "%Y%m%d%H%M%S %z";

/// Time function to parse them from the XMLTV file. Examples:
/// - `20240328102000` for 2024-03-28 10:20:00
/// - `20240328102000 +0100` with a time zone
pub fn parse(value: &str) -> Result<i64, ParseError> {
    let result = if value.len() == 14 {
        NaiveDateTime::parse_from_str(value, PATTERN)?
    } else {
        NaiveDateTime::parse_from_str(value, PATTERN_WITH_TZ)?
    };
    Ok(result.and_utc().timestamp())
}

/// Check if the `value` is in the given interval (`start` and `end`). All values are an `i64` timestamp.
#[inline]
pub fn start_in_range(value: i64, start: i64, end: i64) -> bool {
    start <= value && value <= end
}

#[cfg(test)]
mod tests {
    use super::{parse, start_in_range};

    #[test]
    fn test_parse_datetime() {
        let mut t: i64 = parse("20240328103400").unwrap();
        assert_eq!(t, 1711622040);

        t = parse("20240328103400 +0100").unwrap();
        assert_eq!(t, 1711622040);
    }

    #[test]
    fn test_start_in_range() {
        let start = parse("20240328100000").unwrap();
        let end = parse("20240328110000").unwrap();

        // in range
        let mut value = parse("20240328101500").unwrap();
        assert!(start_in_range(value, start, end));
        // bounds
        value = parse("20240328100000").unwrap();
        assert!(start_in_range(value, start, end));
        value = parse("20240328110000").unwrap();
        assert!(start_in_range(value, start, end));

        // before
        value = parse("20240328095000").unwrap();
        assert!(!start_in_range(value, start, end));

        // after
        value = parse("20240328143000").unwrap();
        assert!(!start_in_range(value, start, end));
    }
}
