use quick_xml::{de::from_str, se::to_string_with_root};
use xmltv::*;

#[test]
fn test_rating() {
    let expected = "<rating system=\"VCHIP\"><value>TV-G</value></rating>";
    let item: Rating = from_str(expected).unwrap();
    assert_eq!(
        item,
        Rating {
            value: "TV-G".to_owned(),
            icons: Vec::with_capacity(0),
            system: Some("VCHIP".to_string())
        }
    );
    assert_eq!(expected, to_string_with_root("rating", &item).unwrap());

    // without system
    let expected = "<rating><value>TV-G</value></rating>";
    let item: Rating = from_str(expected).unwrap();
    assert_eq!(
        item,
        Rating {
            value: "TV-G".to_owned(),
            icons: Vec::with_capacity(0),
            system: None
        }
    );
    assert_eq!(expected, to_string_with_root("rating", &item).unwrap());

    // another case
    let expected = "<rating system=\"CSA\"><value>-12</value><icon src=\"http://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Moins12.svg/200px-Moins12.svg.png\"/></rating>";
    let item: Rating = from_str(expected).unwrap();
    assert_eq!(
        item,
        Rating {
            value: "-12".to_owned(),
            icons: vec![Icon { src: "http://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Moins12.svg/200px-Moins12.svg.png".to_owned(), ..Default::default() }],
            system: Some("CSA".to_string())
        }
    );
    assert_eq!(expected, to_string_with_root("rating", &item).unwrap());
}

#[test]
fn test_star_rating() {
    let expected = "<star-rating><value>3/5</value></star-rating>";
    let item: StarRating = from_str(expected).unwrap();
    assert_eq!(
        item,
        StarRating {
            value: "3/5".to_owned(),
            ..Default::default()
        }
    );
    assert_eq!(expected, to_string_with_root("star-rating", &item).unwrap());
}

#[test]
fn test_episode_num() {
    let expected = "<episode-num system=\"xmltv_ns\">5.11.</episode-num>";
    let item: EpisodeNum = from_str(expected).unwrap();
    assert_eq!(
        item,
        EpisodeNum {
            value: "5.11.".to_owned(),
            system: "xmltv_ns".to_owned()
        }
    );
    assert_eq!(expected, to_string_with_root("episode-num", &item).unwrap());
}

#[test]
fn test_audio() {
    let expected = "<audio><stereo>stereo</stereo></audio>";
    let item: Audio = from_str(expected).unwrap();
    assert_eq!(
        item,
        Audio {
            present: None,
            stereo: Some(TagWithOnlyText {
                value: "stereo".to_string()
            })
        }
    );
    assert_eq!(expected, to_string_with_root("audio", &item).unwrap());
}

#[test]
fn test_credits() {
    let expected = "<credits><director>Bart Eskander</director><producer>Roger Dobkowitz</producer><presenter>Drew Carey</presenter></credits>";
    let item: Credits = from_str(expected).unwrap();
    assert_eq!(
        item,
        Credits {
            directors: vec!["Bart Eskander".to_string()],
            producers: vec!["Roger Dobkowitz".to_string()],
            presenters: vec!["Drew Carey".to_string()],
            ..Default::default()
        }
    );
    assert_eq!(expected, to_string_with_root("credits", &item).unwrap());

    let expected =
        "<credits><director>David Hourrègue</director><actor>Sami Bouajila</actor></credits>";
    let item: Credits = from_str(expected).unwrap();
    assert_eq!(
        item,
        Credits {
            directors: vec!["David Hourrègue".to_string()],
            actors: vec![Actor {
                name: "Sami Bouajila".to_owned(),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(expected, to_string_with_root("credits", &item).unwrap());
}

#[test]
fn test_subtitles() {
    let expected = "<subtitles type=\"teletext\"/>";
    let item: Subtitles = from_str(expected).unwrap();
    assert_eq!(
        item,
        Subtitles {
            language: None,
            r#type: Some("teletext".to_string())
        }
    );
    assert_eq!(expected, to_string_with_root("subtitles", &item).unwrap());
}

#[test]
fn test_value_and_lang() {
    let expected = "<title lang=\"en\">The Young and the Restless</title>";
    let item: ValueAndLang = from_str(expected).unwrap();
    assert_eq!(
        item,
        ValueAndLang {
            value: "The Young and the Restless".to_owned(),
            lang: Some("en".to_string())
        }
    );
    assert_eq!(expected, to_string_with_root("title", &item).unwrap());

    let expected = "<sub-title>Sabrina Offers Victoria a Truce</sub-title>";
    let item: ValueAndLang = from_str(expected).unwrap();
    assert_eq!(
        item,
        ValueAndLang {
            value: "Sabrina Offers Victoria a Truce".to_owned(),
            lang: None
        }
    );
    assert_eq!(expected, to_string_with_root("sub-title", &item).unwrap());

    let expected = "<premiere/>";
    let item: ValueAndLang = from_str(expected).unwrap();
    assert_eq!(
        item,
        ValueAndLang {
            value: String::with_capacity(0),
            lang: None
        }
    );
    assert_eq!(expected, to_string_with_root("premiere", &item).unwrap());
}

#[test]
fn test_length() {
    let expected = "<length units=\"minutes\">64</length>";
    let item: Length = from_str(expected).unwrap();
    assert_eq!(
        item,
        Length {
            length: 64,
            units: Units::Minutes
        }
    );
    assert_eq!(expected, to_string_with_root("length", &item).unwrap());
}

#[test]
fn test_category() {
    let expected = "<category lang=\"fr\">série historique</category>";
    let item: NameAndLang = from_str(expected).unwrap();
    assert_eq!(
        item,
        NameAndLang {
            lang: Some("fr".to_string()),
            name: "série historique".to_owned()
        }
    );
    assert_eq!(expected, to_string_with_root("category", &item).unwrap());
}

#[test]
fn test_previously_shown() {
    let expected = "<previously-shown/>";
    let item: PreviouslyShown = from_str(expected).unwrap();
    assert_eq!(item, PreviouslyShown::default());
    assert_eq!(
        expected,
        to_string_with_root("previously-shown", &item).unwrap()
    );
}

#[test]
fn test_parsing_various_programmes() {
    let content = "<programme start=\"20240305045700\" channel=\"C4.api.telerama.fr\" stop=\"20240305052900\">
    <title>Le 6h info</title>
    <desc lang=\"fr\">Un rendez-vous réveil-matin, avec un point sur l'actualité assorti de différentes rubriques qui permettent d'en explorer certains aspects plus en profondeur.</desc>
    <category lang=\"fr\">journal</category>
    <length units=\"minutes\">32</length>
    <icon src=\"https://focus.telerama.fr/1111x625/2023/09/13/44bdd201e56130105e698b4f3765757ba75900f0.jpg\" />
    <new />
    <rating system=\"CSA\">
      <value>Tout public</value>
    </rating>
  </programme>";
    let _result: Tv = from_str(content).unwrap();

    let content = "<programme start=\"20240305194700\" channel=\"C4.api.telerama.fr\" stop=\"20240305201000\">
    <title>Un si grand soleil</title>
    <desc lang=\"fr\">Une femme revient à Montpellier après des années d'absence, afin de faire découvrir sa ville natale à son fils adolescent. Elle se retrouve accusée de meurtre.</desc>
    <credits>
       <actor>Emma Colberti</actor>
       <actor>Tonya Kinzinger</actor>
       <actor>Frédéric Van</actor>
       <actor>Chrystelle Labaude</actor>
       <actor>Yvon Back</actor>
       <actor>Nadia Fossier</actor>
       <actor>Mélanie Maudran</actor>
       <actor>Fabrice Deville</actor>
       <actor>Marie-Gaëlle Cals</actor>
       <actor>Aurore Delplace</actor>
       <actor>Moïse Santamaria</actor>
       <actor>Benjamin Bourgois</actor>
       <writer>Szulzynger Olivier</writer>
       <writer>Stéphanie Tchou-Cotta</writer>
       <writer>Cristina Arellano</writer>
       <writer>Eline Le</writer>
    </credits>
    <category lang=\"fr\">feuilleton policier</category>
    <length units=\"minutes\">23</length>
    <icon src=\"https://focus.telerama.fr/1111x625/2023/04/18/4a2fdbecb8f5224a1b9227cf2b533b19e311f123.jpg\" />
    <premiere />
    <rating system=\"CSA\">
       <value>Tout public</value>
    </rating>
 </programme>";
    let _result: Tv = from_str(content).unwrap();

    let content = "<programme start=\"20240305201000\" channel=\"C4.api.telerama.fr\" stop=\"20240305214800\">
    <title>L'événement</title>
    <desc lang=\"fr\">Issue d'une famille modeste, Anne compte sur les études pour profiter de l'ascenseur social et s'offrir un avenir plus radieux. Agée de 23 ans, la jeune femme voit son monde s'écrouler quand elle apprend qu'elle attend un enfant. Pour que ses rêves ne se transforment pas en illusions perdues, elle conclut que l'avortement se révèle comme l'unique solution. En cette année 1963, la pilule contraceptive n'en est qu'à ses balbutiements et aucune loi n'encadre cet acte considéré comme immoral. Voyant sa demande fermement rejetée par son gynécologue et par les médecins, Anne se tourne vers des \"spécialistes\" qui font fi des règles... - Critique : Brillante étudiante en lettres à l’orée des années 1960, Anne, d’origine modeste, découvre qu’elle est enceinte. Commence alors pour la jeune fille un parcours de combattante, solitaire et obstinée, pour avorter clandestinement, à une époque où l’avortement est illégal et souvent létal.

Avec son deuxième film, Lion d’or à la Mostra de Venise, Audrey Diwan frappe fort en faisant sien le livre autobiographique d’Annie Ernaux. Elle offre aux mots neutres et crus de l’écrivaine une vraie chair de cinéma, en s’attachant à chaque pas de son héroïne grâce à un cadrage serré qui laisse, souvent, le reste du monde, flou, à l’arrière-plan. D’Anne, elle filme le dos, la nuque et nous place dans sa perspective affolée, avançant comme en territoire ennemi.

Elle filme aussi, bien sûr, le merveilleux visage, buté ou déformé par la douleur, de l’impressionnante Anamaria Vartolomei. En un compte à rebours angoissant s’égrènent les semaines de grossesse. Jamais film n’avait accompagné ainsi, jusqu’au bout, jusqu’à l’insoutenable, une héroïne soumise à la violence physique et psychologique d’un avortement clandestin. Mais avec, à chaque fois, des plans respectueux, dans la douleur comme dans la grâce. Par la singularité de sa reconstitution d’époque, à la fois précise et atmosphérique, L’Événement s’impose comme un film d’une totale modernité sur une fille enfin propriétaire de son corps.</desc>
    <credits>
      <actor>Sandrine Bonnaire</actor>
      <actor>Anna Mouglalis</actor>
      <actor>Alice de</actor>
      <writer>Marcia Romano</writer>
      <writer>Audrey Diwan</writer>
    </credits>
    <date>2021</date>
    <category lang=\"fr\">film : drame</category>
    <length units=\"minutes\">98</length>
    <icon src=\"https://focus.telerama.fr/1111x625/2023/01/02/a2ba2d300e6092e946041a377972a8a44ef94ec7.jpg\" />
    <rating system=\"CSA\">
      <value>-10</value>
      <icon src=\"http://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Moins10.svg/200px-Moins10.svg.png\" />
    </rating>
    <star-rating>
      <value>4/5</value>
    </star-rating>
  </programme>";
    let _result: Tv = from_str(content).unwrap();
}
