# xmltv

XMLTV for electronic program guide (EPG) parser and generator using serde.

## Features

- Flatten some elements like `<new/>` to be a boolean in order to avoid boilerplate
- CLI available to perform some operations

## Usage

### Library

```rust
use std::str::FromStr;

use quick_xml::de::from_str;
use quick_xml::se::to_string_with_root;
use xmltv::*;

fn main() {
    let xml = "<tv>\
<programme channel=\"1\" start=\"2021-10-09 12:00:00 +0200\" stop=\"2021-10-09 13:00:00 +0200\">\
<title>Les feux de l&apos;amour é</title>\
</programme>\
<programme channel=\"2\" start=\"2021-10-09 12:20:00 +0200\" stop=\"2021-10-09 12:35:00 +0200\">\
<title lang=\"fr-FR\">Le journal</title>\
</programme>\
<programme channel=\"3\" start=\"2021-10-09 13:00:00 +0200\" stop=\"2021-10-09 13:40:00 +0200\">\
<title>Le journal</title>\
</programme>\
</tv>";

    // deserialize into the root object (Tv)
    let item: Tv = from_str(xml).unwrap();

    // serialize into string
    let _out = to_string_with_root("tv", &tv).unwrap();
}
```

To serialize **big files**, you can look into `tests/from_files.rs::test_programmes_from_files`.
It reads programmes one by one without loading everything in RAM.

### Command line interface (CLI)

Install it with `cargo install xmltv` or with the binaries.

- `-c <CHANNEL LIST>` or `--channels=<CHANNEL LIST>`: channels you want to keep, ie: `-c tf1,m6,Arte`. You can also check them with the TUI using `-c ?`
- `--cleanse <ITEM LIST>`: items you want to remove form the XMLTV file. I made this option because, in XMLTV file there are a lot of data and we don't need all of them and we may want to reduce the size of the file. Available items are:
  - `credits`: remove all credits. If you want to remove some of them, use:
    - `directors`
    - `actors`
    - `writers`
    - `adapters`
    - `producers`
    - `composers`
    - `editors`
    - `presenters`
    - `commentators`
    - `guests`
  - `categories`
  - `new`
  - `countries`
  - `keywords`
  - `sub-titles`
  - `languages`
  - `videos`
  - `audios`
  - `subtitles`
  - `last-chances`
  - `descriptions`
  - `dates`
  - `origin-languages`
  - `length`
  - `premieres`
  - `previously-showns`
  - `ratings`
  - `star-ratings`
  - `reviews`
  - `images`
  - `episode-nums`
  - `icons`
  - `urls`
- `-d NUMBER` or `--days NUMBER`: number of days in the EPG to keep
- `-o <FILE>` or `--output <FILE>`: new XMLTV file you want to generate otherwise it is printed in the console.

## Roadmap

- [ ] CLI
  - [ ] limit to *N* days `-d <number>`. `-d 3` means from today and the nex 3 days
  - [ ] clean: remove some unwanted elements to make it lighter. You can remove description `--no-description`,
  all credits `--no-credits` or partial one (`--no-directors`, `--no-actors`, `--no-guests`, ...), `--no-categories`,
  `--no-keywords`, `--no-countries`, ... You can also remove unwanted time slot(s) (like between 0:00-6:00 and/or
  23:30-23:59) or unwanted day(s) `--days 1,2,3,5,6,7` (here no thursday, first day is monday).
  - [ ] load external filter (`-l <FILE>` or `--load <FILE>`) from a configuration file to avoid a command with too many arguments or just to follow
  favorite programmes.
  - [x] check channels to filter
  - [ ] search in the title (and maybe other like keywords, description, credits)
  - [ ] show EPG
  - [ ] configure favorites and filters?

## Ressources

XMLTV DTD is here: https://github.com/XMLTV/xmltv/blob/master/xmltv.dtd
